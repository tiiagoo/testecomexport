package com.br.comexport.tiago;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.br.comexport.tiago.domain.ContaContabil;
import com.br.comexport.tiago.domain.LancamentoContabil;
import com.br.comexport.tiago.dto.LancamentoContabilDTO;
import com.br.comexport.tiago.repository.ContaContabilRepository;
import com.br.comexport.tiago.repository.LancamentoContabilRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LancamentoContabilResourceTest extends BaseEndpointTest {

	@Autowired
	LancamentoContabilRepository lancamentoRepository;

	@Autowired
	ContaContabilRepository contaRepository;

	LancamentoContabil lancamento;
	LancamentoContabil lancamentoContabilPost;
	ContaContabil contaContabil;

	@Before
	public void setup() {
		super.setup();

		this.contaContabil = new ContaContabil(null, "Conta Contabil Teste");
		this.lancamento = new LancamentoContabil(new Date(), 25000.15, contaContabil);
		this.lancamentoContabilPost = new LancamentoContabil(new Date(), 250.00, this.contaContabil);

		contaRepository.saveAll(Arrays.asList(contaContabil));
		lancamentoRepository.saveAll(Arrays.asList(lancamento));
	}

	@After
	public void clear() {
		contaRepository.deleteById(this.contaContabil.getNumero());
	}

	@Test
	public void testFindLancamentosContabeis_success() throws Exception {

		String id = lancamento.getId();

		mockMvc.perform(get("/lancamentos-contabeis/{id}", id)).andExpect(status().isOk())
				.andExpect(content().contentType(JSON_MEDIA_TYPE)).andReturn();

	}

	@Test
	public void testFindLancamentosContabeis_failed() throws Exception {

		mockMvc.perform(get("/lancamentos-contabeis/{id}", "100")).andExpect(status().isNotFound())
				.andExpect(content().contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.msg", is(
						"Lançamento Contabil não encontrado pelo Id: 100, Tipo: com.br.comexport.tiago.domain.LancamentoContabil is not exported")))
				.andReturn();

	}

	@Test
	public void testFindLancamentosContabeisByContaContabil_success() throws Exception {

		Long numero = this.contaContabil.getNumero();

		mockMvc.perform(get("/lancamentos-contabeis/").param("contaContabil", numero.toString()))
				.andExpect(status().isOk()).andExpect(content().contentType(JSON_MEDIA_TYPE)).andReturn();

	}

	@Test
	public void testFindLancamentosContabeisByContaContabil_failed() throws Exception {

		Long numero = 100L;

		mockMvc.perform(get("/lancamentos-contabeis/").param("contaContabil", numero.toString()))
				.andExpect(status().isNotFound()).andExpect(content().contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.msg", is(
						"Conta Contabil não encontrada pelo numero: 100 , Tipo: com.br.comexport.tiago.domain.ContaContabil is not exported")))
				.andReturn();
	}

	@Test
	public void testFindLancamentosStatistics_success() throws Exception {

		mockMvc.perform(get("/lancamentos-contabeis/_stats/")).andExpect(status().isOk())
				.andExpect(content().contentType(JSON_MEDIA_TYPE)).andReturn();
	}

	@Test
	public void testFindLancamentosStatisticsByContaContabil_success() throws Exception {

		Long numero = this.contaContabil.getNumero();

		mockMvc.perform(get("/lancamentos-contabeis/_stats/").param("contaContabil", numero.toString()))
				.andExpect(status().isOk()).andExpect(content().contentType(JSON_MEDIA_TYPE)).andReturn();
	}

	@Test
	public void testFindLancamentosStatisticsByContaContabil_failed() throws Exception {

		Long numero = 100L;

		mockMvc.perform(get("/lancamentos-contabeis/_stats/").param("contaContabil", numero.toString()))
				.andExpect(status().isNotFound()).andExpect(content().contentType(JSON_MEDIA_TYPE))
				.andExpect(jsonPath("$.msg", is(
						"Conta Contabil não encontrada pelo numero: 100 , Tipo: com.br.comexport.tiago.domain.ContaContabil is not exported")))
				.andReturn();
	}

	@Test
	public void testCreateLancamentoContabil_success() throws Exception {

		LancamentoContabilDTO dto = new LancamentoContabilDTO(this.lancamentoContabilPost);
		String content = json(dto);

		mockMvc.perform(
				post("/lancamentos-contabeis/").accept(JSON_MEDIA_TYPE).content(content).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().isCreated());
	}

	@Test
	public void testCreateLancamentoContabilDateIsNull_failed() throws Exception {

		LancamentoContabil lancamentoContabilPost = new LancamentoContabil(null, 250.00, this.contaContabil);
		LancamentoContabilDTO dto = new LancamentoContabilDTO(lancamentoContabilPost);
		String content = json(dto);

		mockMvc.perform(
				post("/lancamentos-contabeis/").accept(JSON_MEDIA_TYPE).content(content).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$.erros[?(@.fieldName == 'data')].message",
						hasItem("Obrigatório informar data de lançamento contabil")));
	}

	@Test
	public void testCreateLancamentoContabilValueIsNull_failed() throws Exception {

		LancamentoContabil lancamentoContabilPost = new LancamentoContabil(new Date(), null, this.contaContabil);
		LancamentoContabilDTO dto = new LancamentoContabilDTO(lancamentoContabilPost);
		String content = json(dto);

		mockMvc.perform(
				post("/lancamentos-contabeis/").accept(JSON_MEDIA_TYPE).content(content).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$.erros[?(@.fieldName == 'valor')].message",
						hasItem("Obrigatório informar valor de lançamento contabil")));
	}

	@Test
	public void testCreateLancamentoContabilValorIsNull_failed() throws Exception {

		LancamentoContabil lancamentoContabilPost = new LancamentoContabil(new Date(), null, this.contaContabil);
		LancamentoContabilDTO dto = new LancamentoContabilDTO(lancamentoContabilPost);
		String content = json(dto);

		mockMvc.perform(
				post("/lancamentos-contabeis/").accept(JSON_MEDIA_TYPE).content(content).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("$.erros[?(@.fieldName == 'valor')].message",
						hasItem("Obrigatório informar valor de lançamento contabil")));
	}

	@Test
	public void testCreateLancamentoContabilNumberContaContabilIsNull_failed() throws Exception {

		LancamentoContabil lancamentoContabilPost = new LancamentoContabil(new Date(), 100.00, new ContaContabil());
		LancamentoContabilDTO dto = new LancamentoContabilDTO(lancamentoContabilPost);
		String content = json(dto);

		mockMvc.perform(
				post("/lancamentos-contabeis/").accept(JSON_MEDIA_TYPE).content(content).contentType(JSON_MEDIA_TYPE))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.erros[?(@.fieldName == 'contaContabil')].message",
						hasItem("Obrigatório informar numero de conta contabil")));
	}

}
