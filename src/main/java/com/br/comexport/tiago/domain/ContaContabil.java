package com.br.comexport.tiago.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ContaContabil implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long numero;

	private String descricao;

	@JsonIgnore
	@OneToMany(mappedBy = "contaContabil", cascade=CascadeType.ALL)
	private List<LancamentoContabil> lancamentosContabeis = new ArrayList<>();

	public ContaContabil() {
	}

	public ContaContabil(Long numero, String descricao) {
		super();
		this.numero = numero;
		this.descricao = descricao;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

	public List<LancamentoContabil> getLancamentosContabeis() {
		return lancamentosContabeis;
	}

	public void setLancamentosContabeis(List<LancamentoContabil> lancamentosContabeis) {
		this.lancamentosContabeis = lancamentosContabeis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaContabil other = (ContaContabil) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}
}
