package com.br.comexport.tiago.resource;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.br.comexport.tiago.domain.LancamentoContabil;
import com.br.comexport.tiago.dto.LancamentoContabilDTO;
import com.br.comexport.tiago.dto.LancamentoContabilStatsDTO;
import com.br.comexport.tiago.service.LancamentoContabilService;

import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/lancamentos-contabeis/")
public class LancamentoContabilResource {

	@Autowired
	private LancamentoContabilService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody LancamentoContabilDTO lancamentoDto)
			throws ObjectNotFoundException {
		LancamentoContabil lancamento = service.fromDtoToObj(lancamentoDto);
		lancamento = service.insert(lancamento);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(lancamento.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<LancamentoContabilDTO> find(@PathVariable String id) throws ObjectNotFoundException {
		LancamentoContabilDTO lancamentoContabilDTO = new LancamentoContabilDTO(service.find(id));
		return ResponseEntity.ok(lancamentoContabilDTO);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<LancamentoContabilDTO>> findByContaContabil(
			@RequestParam(name = "contaContabil") Long numero) throws ObjectNotFoundException {
		List<LancamentoContabil> list = service.findByContaContabil(numero);
		List<LancamentoContabilDTO> listDto = list.stream()
				.map(lancamentoContabel -> new LancamentoContabilDTO(lancamentoContabel)).collect(Collectors.toList());
		return ResponseEntity.ok(listDto);
	}

	@RequestMapping(value = "_stats/", method = RequestMethod.GET)
	public ResponseEntity<LancamentoContabilStatsDTO> findStatistics(
			@RequestParam(name = "contaContabil", required = false) Long numero) throws ObjectNotFoundException {
		LancamentoContabilStatsDTO lancamentoStatsDto = new LancamentoContabilStatsDTO(service.findStats(numero));
		return ResponseEntity.ok(lancamentoStatsDto);
	}
}
