package com.br.comexport.tiago.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.br.comexport.tiago.domain.LancamentoContabil;
import com.fasterxml.jackson.annotation.JsonFormat;

public class LancamentoContabilDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Obrigatório informar numero de conta contabil")
	private Long contaContabil;

	@NotNull(message = "Obrigatório informar data de lançamento contabil")
	@JsonFormat(pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East")
	private Date data;

	@NotNull(message = "Obrigatório informar valor de lançamento contabil")
	private Double valor;

	public LancamentoContabilDTO() {
	}

	public LancamentoContabilDTO(LancamentoContabil lancamentoContabil) {
		this.contaContabil = lancamentoContabil.getContaContabil().getNumero();
		this.data = lancamentoContabil.getData();
		this.valor = lancamentoContabil.getValor();
	}

	public Long getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(Long contaContabil) {
		this.contaContabil = contaContabil;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}
