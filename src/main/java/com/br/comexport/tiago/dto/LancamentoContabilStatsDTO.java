package com.br.comexport.tiago.dto;

import java.io.Serializable;
import java.math.RoundingMode;
import java.util.List;

import org.decimal4j.util.DoubleRounder;

import com.br.comexport.tiago.domain.LancamentoContabil;

public class LancamentoContabilStatsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double soma;
	private Double min;
	private Double max;
	private Double media;
	private Long   qtde;

	public LancamentoContabilStatsDTO(List<LancamentoContabil> lancamentos) {
		this.soma 	= lancamentos.stream().mapToDouble(LancamentoContabil::getValor).sum();
		this.min 	= lancamentos.stream().mapToDouble(LancamentoContabil::getValor).min().orElse(0.0);
		this.max 	= lancamentos.stream().mapToDouble(LancamentoContabil::getValor).max().orElse(0.0);
		this.media 	= lancamentos.stream().mapToDouble(LancamentoContabil::getValor).average().orElse(0.0);
		this.qtde 	= lancamentos.stream().count();
	}

	public Double getSoma() {
		return DoubleRounder.round(soma, 2, RoundingMode.FLOOR);
	}

	public void setSoma(Double soma) {
		this.soma = soma;
	}

	public Double getMin() {
		return DoubleRounder.round(min, 2, RoundingMode.FLOOR);
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return DoubleRounder.round(max, 2, RoundingMode.FLOOR);
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Double getMedia() {
		return DoubleRounder.round(media, 2, RoundingMode.FLOOR);
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	public Long getQtde() {
		return qtde;
	}

	public void setQtde(Long qtde) {
		this.qtde = qtde;
	}
}
