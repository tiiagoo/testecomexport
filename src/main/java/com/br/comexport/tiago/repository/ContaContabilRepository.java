package com.br.comexport.tiago.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.comexport.tiago.domain.ContaContabil;

@Repository
public interface ContaContabilRepository extends JpaRepository<ContaContabil, Long> {

}
