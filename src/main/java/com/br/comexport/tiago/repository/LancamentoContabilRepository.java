package com.br.comexport.tiago.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.comexport.tiago.domain.ContaContabil;
import com.br.comexport.tiago.domain.LancamentoContabil;

@Repository
public interface LancamentoContabilRepository extends JpaRepository<LancamentoContabil, String> {

	public List<LancamentoContabil> findByContaContabil(ContaContabil contaContabil);
}
