package com.br.comexport.tiago;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.br.comexport.tiago.domain.ContaContabil;
import com.br.comexport.tiago.domain.LancamentoContabil;
import com.br.comexport.tiago.repository.ContaContabilRepository;
import com.br.comexport.tiago.repository.LancamentoContabilRepository;

@SpringBootApplication
public class TestComexportTiagoGarciaApplication implements CommandLineRunner {

	@Autowired
	private ContaContabilRepository contaContabilRepository;

	@Autowired
	private LancamentoContabilRepository lancamentoContabilRepository;

	public static void main(String[] args) {
		SpringApplication.run(TestComexportTiagoGarciaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		ContaContabil contaContabil1 = new ContaContabil(null, "Conta Contabil Comexport");
		ContaContabil contaContabil2 = new ContaContabil(null, "Conta Contabil Tiago");

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		LancamentoContabil lancamento1 = new LancamentoContabil(sdf.parse("20/03/2018"), 25000.15, contaContabil1);
		LancamentoContabil lancamento2 = new LancamentoContabil(sdf.parse("20/03/2018"), 13040.11, contaContabil1);
		LancamentoContabil lancamento3 = new LancamentoContabil(sdf.parse("21/03/2018"), 5010.98, contaContabil1);
		LancamentoContabil lancamento4 = new LancamentoContabil(sdf.parse("22/03/2018"), 875.03, contaContabil1);
		LancamentoContabil lancamento5 = new LancamentoContabil(sdf.parse("25/03/2018"), 34790.00, contaContabil1);

		LancamentoContabil lancamento6 = new LancamentoContabil(sdf.parse("21/03/2018"), 210.38, contaContabil2);
		LancamentoContabil lancamento7 = new LancamentoContabil(sdf.parse("22/03/2018"), 775.03, contaContabil2);
		LancamentoContabil lancamento8 = new LancamentoContabil(sdf.parse("25/03/2018"), 21790.00, contaContabil2);
		
		contaContabil1.getLancamentosContabeis().addAll(Arrays.asList(lancamento1, lancamento2, lancamento3, lancamento4, lancamento5));
		contaContabil1.getLancamentosContabeis().addAll(Arrays.asList(lancamento6, lancamento7, lancamento8));
		
		contaContabilRepository.saveAll(Arrays.asList(contaContabil1, contaContabil2));
		lancamentoContabilRepository.saveAll(Arrays.asList(lancamento1, lancamento2, lancamento3, lancamento4,
				lancamento5, lancamento6, lancamento7, lancamento8));
	}
}
