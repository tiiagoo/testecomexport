package com.br.comexport.tiago.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.comexport.tiago.domain.ContaContabil;
import com.br.comexport.tiago.repository.ContaContabilRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class ContaContabilService {

	@Autowired
	private ContaContabilRepository repository;

	public ContaContabil find(Long numero) throws ObjectNotFoundException {
		Optional<ContaContabil> lancamentoContabil = repository.findById(numero);
		return lancamentoContabil.orElseThrow(() -> new ObjectNotFoundException(
				"Conta Contabil não encontrada pelo numero: " + numero + " , Tipo: " + ContaContabil.class.getName()));
	}
}
