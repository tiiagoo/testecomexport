package com.br.comexport.tiago.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.comexport.tiago.domain.ContaContabil;
import com.br.comexport.tiago.domain.LancamentoContabil;
import com.br.comexport.tiago.dto.LancamentoContabilDTO;
import com.br.comexport.tiago.repository.LancamentoContabilRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class LancamentoContabilService {

	@Autowired
	private LancamentoContabilRepository repository;

	@Autowired
	private ContaContabilService contaContabilService;

	public LancamentoContabil find(String id) throws ObjectNotFoundException {
		Optional<LancamentoContabil> lancamentoContabil = repository.findById(id);
		return lancamentoContabil
				.orElseThrow(() -> new ObjectNotFoundException("Lançamento Contabil não encontrado pelo Id: " + id
						+ ", Tipo: " + LancamentoContabil.class.getName()));
	}

	public List<LancamentoContabil> findByContaContabil(Long numero) throws ObjectNotFoundException {
		ContaContabil contaContabil = contaContabilService.find(numero);
		return repository.findByContaContabil(contaContabil);
	}

	public List<LancamentoContabil> findStats(Long numero) throws ObjectNotFoundException {
		List<LancamentoContabil> list = new ArrayList<>();
		if (numero != null)
			list = findByContaContabil(numero);
		else
			list = repository.findAll();
		return list;
	}

	public LancamentoContabil fromDtoToObj(@Valid LancamentoContabilDTO lancamentoDto) throws ObjectNotFoundException {
		ContaContabil contaContabil = contaContabilService.find(lancamentoDto.getContaContabil());
		LancamentoContabil lancamentoContabil = new LancamentoContabil(lancamentoDto.getData(), lancamentoDto.getValor(), contaContabil);
		return lancamentoContabil;
	}

	public LancamentoContabil insert(LancamentoContabil lancamento) {
		lancamento = repository.save(lancamento);
		return lancamento;
	}
}
